# Part Definitions, and How to Read Them
A part is defined by a couple of pieces of info:

1. The name, level, and part type.
2. The effects the part can have:
	2a. The cost of activating the effect...
	2b. And what happens when it activates.

An example part might have, for instance:

* [1] Machinegun 1/2/3: Gun
	- [2a] 2 Power, Lock: [2b] Roll Auto 1/2/2 and deal 1/1/2 damage

Machinegun is a "Gun" part, and comes in level 1, 2, and 3 variants. It costs 2 power to activate, and locks when it activates. On activation, you "Roll" for an attack, with the Auto modifier, and deal damage according to the part's level.

Note that wherever a part has different values per level, it can be represented the same way that multiple levels are, and each digit represents the value at the corresponding level. For instance, damage of 1/2/3 means that the gun does 1 damage at level 1, 2 damage at level 2, and 3 damage at level 3.

# Virtual parts versus Physical parts

Physical parts and virtual parts only differ in who can use them (humans can pick up a physical part and use it manually, but require a CBI to use virtual parts), and what power type they use. Physical parts use Power, which is provided by physical generators. Virtual parts use Cycles, which is provided by virtual generators. Humans do not use Power or Cycles; instead they use Beats, which are generated by their heart. Beats may be used to power either physical parts or virtual parts.


# Tags
Tags affect how a part functions; they can change which parts your gun will hit, cause it to repair the target instead of damaging it, lock and unlock parts, apply nudges to your weapons automatically, and do various other things. Tags override existing behavior unless otherwise noted. Some tags obviously don't work alongside certain others; where there appears to be conflict, the tag furthest to the right takes precedence, and you should let someone know that the part has broken tags.

Where a tag has [X] in front of it, it is expected to have a number attached. This number affects how the tag behaves. This number can *also* be level-dependent. If a tag has 0 as its value for a given level, then it does not have that tag for that level.

## List of tags

A -

Automatic [X]:
When attacking with this weapon, roll an additional X attacks and resolve them in the same attack action. In total, you should be rolling X+1 dice. These additional attacks trigger tags which have effects when attacking and after attacking.

Armor [X]:
When an opponent attacks the Cyber this part is attached to, any attack that hits within X slots of this part instead hits this part. Attacks are resolved from lowest slot-number to highest slot-number. Multi-hit attacks, such as Explode or Split, will hit Armor multiple times when they hit multiple slots covered by Armor.

B -

Bomb:
Instead of rolling, pick a number on the dice. Write down this number on a piece of scratch paper, along with which part you're attacking with, and who you are targeting. If your target rolls that number, then their attack is interrupted, and they are hit by the part you created the 'bomb' with, as if you had rolled that number. You may nudge your roll *after* you hit, but not when determining if you hit.

C -

Copy:
This part creates a new object with its own stat sheet. The thing created will either be specified in the part, or must be a certain kind of object (i.e., some viruses have parts which Copy any virus).

D -

Direct:
When attacking with this weapon, the weapon hits the part slot you rolled. This is the default behavior; however, some tags replace this behavior, and you can add this tag to explicitly use both this tag's behavior *and* the other tag's effect.

Drain [X]:
This part removes X Power from physical targets, and X Cycles from virtual targets. Physical parts may only drain Power, and Virtual parts may only drain Cycles.

E -

Expose:
After attacking with this weapon, pick a part slot on your opponent. Your opponent must tell you if that part slot has something in it, and what type of part it is.

F -

Finesse [X]:
Nudge your roll by *up to* X slots. Found more often on dedicated aim-adjust parts.

G -

H -

I -

J -

Janky:
If this weapon does not hit, then it does not unlock during your unlock step. You will need to find another way to unlock it. It will unlock at the end of combat otherwise.

K -

Klunky:
If this weapon is damaged, then it does not unlock during your unlock step. You will need to find another way to unlock it. It will unlock at the end of combat otherwise.

L -

Lock:
Any parts affected by this weapon are Locked. They do not unlock during the target's next unlock step. Not to be confused with locking as part of the activation cost for a part.

M -

Multi-target [X]:
You may pick up to X additional targets to use this part on. Use the same roll for each target. I.e., if your part has Multi-target 3, you only roll one die and use the same value for all three targets.

N -

Nudge [X]:
*Always* nudge your roll by *exactly* X slots, even if nudging would make you miss. If you want finer control over your aim, you'll need something with Finesse instead.

O -

Overclock [X]:
This part adds X power to physical targets, and X cycles to virtual targets. Physical parts may only add power, and virtual parts may only add cycles. This can trigger Overheating.

P -

Precise [X]:
When rolling for a part slot, roll X additional dice and choose one of the results.

Q -

Quantum [X]:
After activating this part, you may move it up to X slots. It keeps any damage it has accrued. If the slot you're moving the part to is occupied, then the parts switch places.

R -

Repair:
Parts affected by this weapon recover HP equal to the weapon's damage value. This affects any target, friend or foe.

S -

Split [X]:
When attacking with this weapon, the weapon hits the part slots at N+X and N-X, where N is the number you rolled. Note that this means you *DO NOT* hit the part slot you rolled; if you want a weapon that hits three different slots, you need both S and D.

T -

Target's Choice:
Instead of rolling a die, your opponent picks a non-empty, non-destroyed part slot. That part slot is affected by this part.

U -

Unlock:
If the affected part was locked, it is unlocked.

V -

Vampire [X]:
This weapon removes X Power or Cycles from the target, and adds X Power or Cycles to the user. Physical parts may only Vampire Power, and Virtual parts may only Vampire Cycles.

W -

X -

eXplode [X]:
This weapon hits the part slot you rolled as well as the parts within X slots of it. An eXplode 1 weapon hits the part you rolled, the part above it, and the part below it.

Y -

Z -

Ztun:
This weapon does not kill Humans and does not damage robots or cybermen. It may still apply tag effects such as Overclock.

! -

spec!al:
This part has a special trait which only the GM knows about. Reserved for very, very important effects that are intended to be used for key story moments.

# Parts List

## Physical parts

### Power
* Fusion Core 1/2/3: Core
 	- Lock: Generate 1/3/5 power.
* Gas Turbine 1/2/3: Core
	- Lock: Generate 2/4/6 power. While this part is locked, activating a part to Nudge a roll costs 1 extra power.
* Solar Panel 1/2/3: Core
	- Generates 1 power. It's good for the environment, but inconvenient.
* Nuclear Ramjet 1/2/3: Core
	- Lock: Generate 2/4/6 power. While this part is locked, activating Gun parts costs 1 extra power.
* Battery 1/2/3: Storage
	- Stores 1/3/5 power.
* Capacitor 1/2/3: Storage
	- Stores 3/5/7 power. During your Upkeep step, lose 1/3/5 power.
* Fuel Cell 1/2/3: Storage
	- Stores 1/2/3 power. While this part is not destroyed: Reduce the cost of Sword parts by 1.
* High-Output Power Unit: Storage
	- Stores 1/2/3 power. While this part is not destoryed: Reduce the cost of Gun parts by 1.

### Guns
* Machinegun 1/2/3: Gun
	- 2 Power, Lock: Roll Auto 1/2/2 and deal 1/1/2 damage
* Shotgun 1/2/3: Gun
	- 2 power, Lock: Roll Multi-target 1/2/3 with Split 1 and deal 1 damage
* Pistol 1/2/3: Gun
	- 1 power, Lock: Roll, Nudge 1/2/2 then deal 1/1/2 damage
* Rifle 1/2/3: Gun
	- 2 power, Lock: Roll Precision, Multi-target 1/1/2 and deal 2 damage. Lock affected parts. Expose 1 part.
* EMP Grenade 1/2/3: Gun
	- 2 power, Lock: Set a bomb with Multi-target 3, doing 3 Stun damage and Exploding with part radius 1/2/3. Targets are Overclocked for 3 power/cycles. (Bombs with Multi-target go off if any one target rolls the target number.)

### Swords
* Tazer 1/2/3: Sword
	- 3 power, Lock: Roll, doing 3 Stun damage and Exploding 1/2/3.
* Baseball Bat 2: 2, Sword 2, L
	- 2 power, Lock: Roll, doing 2 damage and Locking affected parts.
* Knife 1: 1, Sword 1, S1
	- 1 power, Lock: Roll with Split 1, doing 1 damage.

### Nudges
* Scope 1/2/3: Nudge
	- 1 power, Lock: Finesse any of your Gun rolls by 1/2/3.
	- 1 power, Lock: Finesse any Sword roll targeting you by 1/2/3.
* Rollers 1/2/3: Nudge
	- 1 power, Lock: Finesse any of your Sword rolls by 1/2/3.
	- 1 power, Lock: Finesse any Gun roll targeting you by 1/2/3.
* Zoom 1/2/3: Nudge
	- 2 power, Lock: Finesse any of your Gun rolls by 2/3/4.
	- 2 power, Lock: Finesse any Sword roll targeting you by 0/1/2.
* Jets 1/2/3: 2, Nudge your swords by 2/3/4, or enemy guns by 0/1/2.
	- 2 power, Lock: Finesse any of your Sword rolls by 2/3/4.
	- 2 power, Lock: Finesse any Gun roll targeting you by 0/1/2.
* Radar 1/2/3: Nudge
	- 2 power, Lock: Finesse any of your Gun rolls by 0/1/2.
	- 2 power, Lock: Finesse any Sword roll targeting you by 2/3/4.
* Jump Pistons 1/2/3: Nudge
	- 2 power, Lock: Finesse any of your Sword rolls by 0/1/2.
	- 2 power, Lock: Finesse any Gun roll targeting you by 2/3/4.
* Optical Camo 2: Nudge
	- 3 power, Lock: Finesse any Gun roll targeting you by 5.
* Threat Matrix Unit 2: Nudge
	- 3 power, Lock: Finesse any Sword roll targeting you by 5.

## Virtual Parts

### Cycles
* CPU 1/2/3: Core
	-	Lock: Generate 1/3/5 Cycles.
* Cached CPU 1/2/3: Core
	-	Lock: Generate 1/2/3 Cycles. While locked: Activating an I/O part generates 1 Cycle.
* Parallel CPU 1/2/3: Core
	- Lock: Generate 1/2/3 Cycles. While locked: Processing parts gain either Precision 1 or Auto 1 when activated.
* Locked-down CPU 1/2/3: Core
	- Lock: Generate 1/2/3 Cycles. While locked: Crypto parts gain Multi-target +1 when activated.
* RAM 1/2/3: Storage
	- Stores 1/3/5 Cycles.
* Swap Disk 1/2/3: Storage
	-	Stores 3/5/7 Cycles. While not destroyed: You may only use one attack chip per turn.
* Extra Cache 1/2/3: Storage
	- Stores 1/2/3 Cycles. Lock: Generate 1 Cycle, then lock another part (your choice).
* Graphics Coprocessor 1: Storage, Nudge
	- Stores 1 Cycle. Lock: Nudge any attack targeting you by 1.
* Analysis Coprocessor 1: Storage, Nudge
	- Stores 1 Cycle. Lock: Nudge any attack you initiated by 1.

### Miners
* I/O Miner 2: Miner
	-	2 cycles, lock: Pick an enemy part slot. If that slot contains an I/O part, gain 1 CCoin.
* Proc Miner 2: Miner
	-	2 cycles, lock: Pick an enemy part slot. If that slot contains a Proc part, gain 1 CCoin.
* Crypto Miner 2: Miner
	-	2 cycles, lock: Pick an enemy part slot. If that slot contains a Crypto part, gain 1 CCoin.
* Core Miner 2: Miner
	-	2 cycles, lock: Pick an enemy part slot. If that slot contains a Core part, gain 3 CCoin.
* Auto Miner 1: Miner
	-	5 cycles, lock: Pick an enemy part slot. If that slot contains any part, gain 1 CCoin.

### I/O
Speed boosts and slowing enemies.

* DoS 1/2/3: I/O
	-	1 cycles, lock: Roll, doing 0/1/2 damage and locking the affected part.
* DDoS 1/2/3: I/O
	- 1 cycles, lock: Roll with multi-target 2/3/4; do 0/0/1 damage, and lock affected parts.
* Zero Cannon 1/2/3: I/O
	-	3 cycles, lock: Roll, exploding with radius 0/1/2 for 1 damage.
* Dump Out 1/2/3: I/O
	-	2 cycles, lock: Roll with Split 1, doing 0/1/2 damage and locking affected parts.
* Garbage Out 1/2/3: I/O
	-	3 cycles, lock: Roll with direct and Split 2, doing 0/1/2 damage and locking affected parts.
* Buffer Flush 1/2/3: I/O
	- 2 cycles, lock: 1/2/2 Target(s) choose a part. Explode 0/0/1, unlocking all affected parts.
* Cache Flush 1/2/3: I/O
    - 2 cycles, lock: Target chooses a part. Explode 0/1/2, unlocking all affected parts, then overclock 1. 
* High-Speed Bus 1/2/3: I/O
	- 2 cycles, lock: Target chooses a part. Lock that part and Overclock for 1/2/3 Cycles.
* I/O Coprocessor 1/2/3: I/O, Nudge
	-	2 power, lock: Finesse any I/O roll by 1/2/3.

### Processing
Finding enemy weaknesses, and exploiting them.

* MMap 1/2/3: Proc
	- 1 power, lock: Roll with Auto 0/0/1, doing 0/1/1 damage. Then, Expose 1.
* Port Scanner 1/2/3: Proc
	-	2 power, lock: Expose 1/2/3.
* Spoofer 1/2/3: Proc
	- 2 power, lock: Roll with Auto 0/0/1, doing 1/2/2 damage. Then, Expose 1.
* Remote Execution 2: Proc
	-	2 power, lock: Roll, then Nudge 2 and do 3 damage.
* Bouncing Bitflip 2: Proc
	- 3 power, lock: Set a bomb doing 3 damage.
* Password Cracker 2: Proc
	-	3 power, lock: Roll with Precision 1, and do 3 damage.
* Processing Core 1/2/3: Proc, Nudge
	-	2 power, lock: Finesse any Proc roll by 1/2/3.
* Cloud Computation Core 1: Proc, Nudge
	- 1 power, lock: Finesse any *other* unit's roll by 1.

### Crypto
Sealing holes, creating obstacles.

* RSA 1/2/3: Crypto
	- 1 power, lock: Target locks and repairs a chip of their choice for 1 HP. Auto 0/1/2.
* PGP 1/2/3: Crypto
	- 1/2/2 power, lock: Target locks and repairs a chip of their choice for 1/2/3 HP.
* AES 3: Crypto
	-	3 power, lock: Target chooses a chip. Explode 1, locking and repairing all affected chips for 3 HP.
* Key Exchange 2: Crypto
	-	2 power, lock: All attacks against a chosen target will instead target you.
* P2P Tunnel 2: Crypto
	- 3 power, lock: Pick two targets. Attacks which would hit the first target instead hit the second target. Attacks which would hit the second target instead hit the first target. The *original target* nudges as if it is the defender. The new target *does not get to nudge*.
* Signature Clone 2: Crypto
	-	2 power, lock: Copy a "Shadow File", which has MMap 1, CPU 1, and Swap Disk 1. You may arrange these parts as you wish.
* FSCK 2: Crypto
	-	3 power, lock: Copy a "Disk Checker", which has PGP 1, Locked-down CPU 1, and Swap Disk 1. You may arrange these parts as you wish.
* Crypto Core 1/2/3: Crypto, Nudge
	- 2 power, lock: Finesse any Crypto roll by 1/2/3.
