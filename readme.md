# Cyberman: Battle Gateway
(A game about robots and AIs fighting each other.)

## What language is this written in?
English! This is a tabletop game. For now.

## How do I play?
Rules are on the way, but for now:
* Your robot is defined by a 12-slot "parts table"
* When you attack, you roll a d12, and compare to your target's parts table
* If the slot you rolled is empty or contains a destroyed part, you missed!
* If the slot you rolled has a part in it, you deal damage to that part
* Robots and AIs are out of the fight when they can't generate power or can't use any attack/support chips
* Robots have physical parts, AIs have virtual parts, humans are squishy

## Where does this take place?
There's a whole setting in the works, taking inspiration from Megaman: Battle Network, as well as the Shin Megami Tensei games and the themes explored in Deus Ex. Future spoilers will go in a spoilers folder for GMs to look around in.
